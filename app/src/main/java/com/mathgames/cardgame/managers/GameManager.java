package com.mathgames.cardgame.managers;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.widget.BaseAdapter;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.mathgames.cardgame.interfaces.GameListListener;
//import com.mathgames.cardgame.activities.testActivity;
import com.mathgames.cardgame.enums.GameStatus;
import com.mathgames.cardgame.interfaces.CardMoveListener;
import com.mathgames.cardgame.models.Card;
import com.mathgames.cardgame.models.Game;
import com.mathgames.cardgame.models.Movement;
import com.mathgames.cardgame.models.Player;
import com.mathgames.cardgame.utils.AppConstants;
import com.mathgames.cardgame.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by eugurlu on 06/03/16.
 */
public class GameManager {
    public static GameManager ins = new GameManager();

    private Game activeGame;
    private Context currentContext;
    public ArrayList<Game> games = new ArrayList<>();
    private Firebase myFirebaseRef;
    boolean listenersAdded = false;
    public Player me;
    private CardMoveListener listener;
    GameListListener gameListener;

    ValueEventListener gamesListValueListener;
    ValueEventListener currentGameListener;
    ValueEventListener currentGamePlayersListener;
    ValueEventListener currentGameStatusListener;
    ChildEventListener currentGameMovesListener;

    private GameManager() {
        setupAllListener();
    }

    public void resetAll() {

        if (this.activeGame != null) {
            myFirebaseRef.child("games/" + activeGame.gameRef.getKey()).removeEventListener(currentGameListener);
            myFirebaseRef.child("games/" + activeGame.gameRef.getKey() + "/players").removeEventListener(currentGamePlayersListener);
            myFirebaseRef.child("games/" + activeGame.gameRef.getKey() + "/status").removeEventListener(currentGameStatusListener);
            myFirebaseRef.child("games/" + activeGame.gameRef.getKey() + "/movements").removeEventListener(currentGameMovesListener);
        }

        this.activeGame = null;
        this.listenersAdded = false;
        this.me = null;
        this.listener = null;

    }

    public void setupAllListener(){
        gamesListValueListener = new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot snapshot) {
                games.clear();
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    Game game = postSnapshot.getValue(Game.class);
                    game.gameRef = postSnapshot.getRef();
                    games.add(game);
                }
                gameListener.gameListUpdated();
            }

            @Override
            public void onCancelled(FirebaseError error) {
            }

        };

        currentGameListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                GameManager.ins.activeGame = dataSnapshot.getValue(Game.class);
                GameManager.ins.activeGame.gameRef = dataSnapshot.getRef();
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        };

        currentGamePlayersListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() == AppConstants.playerCount) {
                    GameManager.ins.activeGame.gameRef.removeEventListener(this);
                    GameManager.ins.activeGame.gameRef.child("status").setValue(GameStatus.WAITING_FOR_CARDS.getValue());
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        };

        currentGameStatusListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("FireBaseEvent", "Game Status updated " + dataSnapshot);
                if (GameStatus.WAITING_FOR_CARDS.getValue() == (Long) dataSnapshot.getValue()) {
                    myFirebaseRef.removeEventListener(this);
                    Log.d("FireBaseEvent", "Start game");
                    waitLittleAndStartGame();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
            }
        };

        currentGameMovesListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d("Test", s + dataSnapshot);
                Movement move = dataSnapshot.getValue(Movement.class);
                listener.newCardMove(move);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        };
    }

    public void startToListenningAllGameList() {
        myFirebaseRef = new Firebase("https://sweltering-fire-5731.firebaseio.com/");
        myFirebaseRef.child("games").addValueEventListener(gamesListValueListener);
    }

    private void startToListenningCurrentGame() {
        myFirebaseRef.child("games/" + activeGame.gameRef.getKey()).addValueEventListener(currentGameListener);
    }

    private void startToListenningCurrentGamePlayers() {
        myFirebaseRef.child("games/" + activeGame.gameRef.getKey() + "/players").addValueEventListener(currentGamePlayersListener);
    }

    private void startToListenningCurrentGameStatus() {
        myFirebaseRef.child("games/" + activeGame.gameRef.getKey() + "/status").addValueEventListener(currentGameStatusListener);
    }

    public void startToListenningCurrentGamePlayerMovements() {
        myFirebaseRef.child("games/" + activeGame.gameRef.getKey() + "/movements").addChildEventListener(currentGameMovesListener);
    }

    public void myNewCardMovement(Card card, int moveIndex) {
        Movement m = new Movement();
        m.setCid(card.getCardId());
        m.setMoveIndex(moveIndex);
        GameManager.ins.getActiveGame().gameRef.child("movements").getRef().push().setValue(m);
    }

    private void waitLittleAndStartGame() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                startGame();
            }
        }, 1000);
    }

    public void addPlayer(int pIndex) {
        Log.d("ActionListener", "Added new player");
        me = new Player();
        me.setName(Utils.randomString(5));
        me.setPid(Utils.getAndroidUUID(this.currentContext));
        me.setpIndex(pIndex);
        GameManager.ins.getActiveGame().gameRef.child("players").getRef().push().setValue(me);
    }

    public void startGame() {
        //Intent myIntent = new Intent(currentContext, testActivity.class);
        //currentContext.startActivity(myIntent);
    }

    public Context getCurrentContext() {
        return currentContext;
    }

    public void setCurrentContext(Context currentContext) {
        this.currentContext = currentContext;
        Firebase.setAndroidContext(this.currentContext);
    }

    public void joinOneOrCreateNewGame() {
        for (Game game : GameManager.ins.games) {
            if (game.getStatus() == 1 && game.getPlayers().size() < AppConstants.playerCount) {
                GameManager.ins.setActiveGame(game);
                GameManager.ins.addPlayer(game.getPlayers().size());
                break;
            }
        }

        if (GameManager.ins.getActiveGame() == null) {
            Game newGame = new Game();
            newGame.setGid(Utils.randomUUID());
            newGame.setStatus(GameStatus.WAITING_FOR_START.getValue());
            //newGame.setActivePlayerIndex(0);

            me = new Player();
            me.setName(Utils.randomString(5));
            me.setPid(Utils.getAndroidUUID(this.currentContext));
            me.setpIndex(0);

            Map<String, Player> players = new HashMap<String, Player>();
            players.put(Utils.randomUUID(), me);
            newGame.setPlayers(players);

            Firebase gameRef = myFirebaseRef.child("games").getRef().push();
            gameRef.setValue(newGame);
            newGame.gameRef = gameRef;
            GameManager.ins.setActiveGame(newGame);
        }
    }

    public Game getActiveGame() {
        return activeGame;
    }

    public void setActiveGame(Game activeGame) {
        Log.d("Game", "seted new active game");
        this.activeGame = activeGame;
        startToListenningCurrentGame();
        if (!this.listenersAdded) {
            this.listenersAdded = true;
            startToListenningCurrentGameStatus();
            startToListenningCurrentGamePlayers();
        }
    }

    public CardMoveListener getListener() {
        return listener;
    }

    public void setListener(CardMoveListener listener) {
        this.listener = listener;
        GameManager.ins.startToListenningCurrentGamePlayerMovements();
    }

    public GameListListener getGameListener() {
        return gameListener;
    }

    public void setGameListener(GameListListener gameListener) {
        this.gameListener = gameListener;
        GameManager.ins.resetAll();
        GameManager.ins.startToListenningAllGameList();
    }
}

