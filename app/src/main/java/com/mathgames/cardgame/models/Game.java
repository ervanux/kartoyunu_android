package com.mathgames.cardgame.models;

import com.firebase.client.Firebase;
import com.mathgames.cardgame.enums.GameStatus;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by eugurlu on 02/03/16.
 */
public class Game {

    public Firebase gameRef = null;

    private String gid;
    private int status;
    //private int activePlayer;
    private Map<String, Player> players;
    private Map<String, Movement> movements;

    public Game(){}

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Map<String, Player> getPlayers() {
        return players;
    }

    public void setPlayers(Map<String, Player> players) {
        this.players = players;
    }

    public Map<String, Movement> getMovements() {
        return movements;
    }

    public void setMovements(Map<String, Movement> movements) {
        this.movements = movements;
    }

    /*
    public void setActivePlayerIndex(int activePlayer) {
        this.activePlayer = activePlayer;
    }

    public int getActivePlayer() {
        return activePlayer;
    }
    */
}
