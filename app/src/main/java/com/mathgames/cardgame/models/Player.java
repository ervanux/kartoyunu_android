package com.mathgames.cardgame.models;

import java.util.Map;

/**
 * Created by eugurlu on 02/03/16.
 */
public class Player {
    private Map<String, Card> cards;
    private String name;
    private String pid;
    private int pIndex;

    public Player(){}

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getpIndex() {
        return pIndex;
    }

    public void setpIndex(int pIndex) {
        this.pIndex = pIndex;
    }

    public Map<String, Card> getCards() {
        return cards;
    }

    public void setCards(Map<String, Card> cards) {
        this.cards = cards;
    }
}
