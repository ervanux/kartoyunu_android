package com.mathgames.cardgame.models;

/**
 * Created by eugurlu on 02/03/16.
 */
public class Card {
    private int cardId;
    public Card(int cardId) {
        this.cardId = cardId;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }
}
