package com.mathgames.cardgame.models;

/**
 * Created by eugurlu on 02/03/16.
 */
public class Movement {
    private int cid;
    private int moveIndex;

    public Movement(){}

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }


    public int getMoveIndex() {
        return moveIndex;
    }

    public void setMoveIndex(int moveIndex) {
        this.moveIndex = moveIndex;
    }
}
