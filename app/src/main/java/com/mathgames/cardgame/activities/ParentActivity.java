package com.mathgames.cardgame.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.firebase.client.Firebase;
import com.mathgames.cardgame.managers.GameManager;
import com.mathgames.cardgame.models.Game;

public class ParentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GameManager.ins.setCurrentContext(this);
    }

}
