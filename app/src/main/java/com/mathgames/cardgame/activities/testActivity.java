package com.mathgames.cardgame.activities;

import android.content.ClipData;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.mathgames.cardgame.R;
//import com.mathgames.cardgame.interfaces.CardMoveListener;
//import com.mathgames.cardgame.managers.GameManager;
import com.mathgames.cardgame.models.Card;
import com.mathgames.cardgame.models.Movement;
import com.mathgames.cardgame.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public  class testActivity extends AppCompatActivity {

    ////////******FireBase gönderelicek degerler******//////////
    private List<Integer> kullanilanKardlar = new ArrayList();//GameManager.ins.getActiveGame().getMovements() içindeki "cid" kartın numarası oluyor ordan kullanılabilir.
    private List<Integer> kullaniciyaDagitilanKardlar = new ArrayList();//GameManager.ins.getActiveGame().getPlayers() içindeki her playering getCards içinden alınabilir.
    private int kullanilanEnsonKard = 0; //
    ////////*****************************************//////////
    private LinearLayout linearLayoutUst, linearLayoutSag, linearLayoutSol, linearLayoutAlt;
    private TextView ustPuan, solPuan, altPuan, sagPuan;
    private int ustDizi[], altDizi[], sagDizi[], solDizi[];
    private AbsoluteLayout layoutCevap;
    private ImageButton btnUst1, btnUst2, btnUst3, btnUst4, btnUst5, btnUst6, btnUst7, btnUst8, btnUst9, btnUst10, btnUst11, btnUst12, btnUst13, btnUst14, btnUst15, btnUst16, btnUst17, btnUst18, btnUst19, btnUst20;
    private ImageButton btnAlt1, btnAlt2, btnAlt3, btnAlt4, btnAlt5, btnAlt6, btnAlt7, btnAlt8, btnAlt9, btnAlt10, btnAlt11, btnAlt12, btnAlt13, btnAlt14, btnAlt15, btnAlt16, btnAlt17, btnAlt18, btnAlt19, btnAlt20;
    ;
    private ImageButton btnSag1, btnSag2, btnSag3, btnSag4, btnSag5, btnSag6, btnSag7, btnSag8, btnSag9, btnSag10, btnSag11, btnSag12, btnSag13;
    private ImageButton btnSol1, btnSol2, btnSol3, btnSol4, btnSol5, btnSol6, btnSol7, btnSol8, btnSol9, btnSol10, btnSol11, btnSol12, btnSol13;
    private TranslateAnimation animationMasayaKardGonder;
    private ImageView btnAnimasyon;
    private ImageView btnUstAnimasyon, btnAltAnimasyon, btnSagAnimasyon, btnSolAnimasyon;
    private int masaX, masaY;
    private int[] location;

    private int playerId = 1, isMainPlayer = 1, playerCount = 2, playerGameCartCount = 20;
    private int playerIdNewCardMove, playerCardIdNewCardMove, playerCardNumberNewCardMove, playerCardNumberNewCardGet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        this.playerId =  R.id.linearLayoutUst;
        playerCount = AppConstants.playerCount;
        TanimalamaIslemleri(playerCount);
        Events();
        CleanTable();

        //Todo playerid 0 ise main oluyor
        this.playerId =  R.id.linearLayoutUst;//GameManager.ins.me.getpIndex();
        this.playerCount = 2;//GameManager.ins.getActiveGame().getPlayers().size();
        //GameManager.ins.getActiveGame().getPlayers();
        isMainPlayer=1;
        //TODO playerId(linearLayoutUst,linearLayoutAlt,linearLayoutSag,linearLayoutSol),playerCount=(2,3,4) ve isMainPlayer(0,1) bilgileri firebasenden geldikten sonra FirstSetPlayersCardCount() baslatılır
        if (playerCount == 2)
            playerGameCartCount = 20;
        else
            playerGameCartCount = 14;
        //TODO Sadece Main kullanıcısına gider
        if (isMainPlayer == 1) {
            FirstSetPlayersCardCount();

            //TODO ustDizi[],altDizi[],sagDizi[],solDizi[] firebase gidecek tüm kullanıclara dagıtılacak
            //TODO kullanilanKardlar,kullaniciyaDagitilanKardlar,kullanilanEnsonKard firebase gidecek tüm kullanıclara dagıtılacak

        }

        //GameManager.ins.setListener(this);

        //GameManager.ins.myNewCardMovement(new Card(12), 1);

    }


    ///////////////////******************Genel Düzenleme***************//////////////////////////////////
    private void TanimalamaIslemleri(Integer oyuncuSayisi)
    {
        linearLayoutUst = (LinearLayout) findViewById(R.id.linearLayoutUst);
        linearLayoutSag = (LinearLayout) findViewById(R.id.linearLayoutSag);
        linearLayoutSol = (LinearLayout) findViewById(R.id.linearLayoutSol);
        linearLayoutAlt = (LinearLayout) findViewById(R.id.linearLayoutAlt);
        layoutCevap = (AbsoluteLayout) findViewById(R.id.layoutCevap);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        layoutCevap.setOnDragListener(new DragListener());
        LayoutParams lp = layoutCevap.getLayoutParams();
        layoutCevap.setLayoutParams(lp);


        btnUstAnimasyon = (ImageView) findViewById(R.id.btnUstAnimasyon);
        btnAltAnimasyon = (ImageView) findViewById(R.id.btnAltAnimasyon);
        btnSagAnimasyon = (ImageView) findViewById(R.id.btnSagAnimasyon);
        btnSolAnimasyon = (ImageView) findViewById(R.id.btnSolAnimasyon);


        ustPuan = (TextView) findViewById(R.id.ustPuan);
        solPuan = (TextView) findViewById(R.id.solPuan);
        altPuan = (TextView) findViewById(R.id.altPuan);
        sagPuan = (TextView) findViewById(R.id.sagPuan);


        btnUst1 = (ImageButton) findViewById(R.id.btnUst1);
        btnUst2 = (ImageButton) findViewById(R.id.btnUst2);
        btnUst3 = (ImageButton) findViewById(R.id.btnUst3);
        btnUst4 = (ImageButton) findViewById(R.id.btnUst4);
        btnUst5 = (ImageButton) findViewById(R.id.btnUst5);
        btnUst6 = (ImageButton) findViewById(R.id.btnUst6);
        btnUst7 = (ImageButton) findViewById(R.id.btnUst7);
        btnUst8 = (ImageButton) findViewById(R.id.btnUst8);
        btnUst9 = (ImageButton) findViewById(R.id.btnUst9);
        btnUst10 = (ImageButton) findViewById(R.id.btnUst10);
        btnUst11 = (ImageButton) findViewById(R.id.btnUst11);
        btnUst12 = (ImageButton) findViewById(R.id.btnUst12);
        btnUst13 = (ImageButton) findViewById(R.id.btnUst13);
        btnUst14 = (ImageButton) findViewById(R.id.btnUst14);
        btnUst15 = (ImageButton) findViewById(R.id.btnUst15);
        btnUst16 = (ImageButton) findViewById(R.id.btnUst16);
        btnUst17 = (ImageButton) findViewById(R.id.btnUst17);
        btnUst18 = (ImageButton) findViewById(R.id.btnUst18);
        btnUst19 = (ImageButton) findViewById(R.id.btnUst19);
        btnUst20 = (ImageButton) findViewById(R.id.btnUst20);


        btnAlt1 = (ImageButton) findViewById(R.id.btnAlt1);
        btnAlt2 = (ImageButton) findViewById(R.id.btnAlt2);
        btnAlt3 = (ImageButton) findViewById(R.id.btnAlt3);
        btnAlt4 = (ImageButton) findViewById(R.id.btnAlt4);
        btnAlt5 = (ImageButton) findViewById(R.id.btnAlt5);
        btnAlt6 = (ImageButton) findViewById(R.id.btnAlt6);
        btnAlt7 = (ImageButton) findViewById(R.id.btnAlt7);
        btnAlt8 = (ImageButton) findViewById(R.id.btnAlt8);
        btnAlt9 = (ImageButton) findViewById(R.id.btnAlt9);
        btnAlt10 = (ImageButton) findViewById(R.id.btnAlt10);
        btnAlt11 = (ImageButton) findViewById(R.id.btnAlt11);
        btnAlt12 = (ImageButton) findViewById(R.id.btnAlt12);
        btnAlt13 = (ImageButton) findViewById(R.id.btnAlt13);
        btnAlt14 = (ImageButton) findViewById(R.id.btnAlt14);
        btnAlt15 = (ImageButton) findViewById(R.id.btnAlt15);
        btnAlt16 = (ImageButton) findViewById(R.id.btnAlt16);
        btnAlt17 = (ImageButton) findViewById(R.id.btnAlt17);
        btnAlt18 = (ImageButton) findViewById(R.id.btnAlt18);
        btnAlt19 = (ImageButton) findViewById(R.id.btnAlt19);
        btnAlt20 = (ImageButton) findViewById(R.id.btnAlt20);


        btnSag1 = (ImageButton) findViewById(R.id.btnSag1);
        btnSag2 = (ImageButton) findViewById(R.id.btnSag2);
        btnSag3 = (ImageButton) findViewById(R.id.btnSag3);
        btnSag4 = (ImageButton) findViewById(R.id.btnSag4);
        btnSag5 = (ImageButton) findViewById(R.id.btnSag5);
        btnSag6 = (ImageButton) findViewById(R.id.btnSag6);
        btnSag7 = (ImageButton) findViewById(R.id.btnSag7);
        btnSag8 = (ImageButton) findViewById(R.id.btnSag8);
        btnSag9 = (ImageButton) findViewById(R.id.btnSag9);
        btnSag10 = (ImageButton) findViewById(R.id.btnSag10);
        btnSag11 = (ImageButton) findViewById(R.id.btnSag11);
        btnSag12 = (ImageButton) findViewById(R.id.btnSag12);
        btnSag13 = (ImageButton) findViewById(R.id.btnSag13);


        btnSol1 = (ImageButton) findViewById(R.id.btnSol1);
        btnSol2 = (ImageButton) findViewById(R.id.btnSol2);
        btnSol3 = (ImageButton) findViewById(R.id.btnSol3);
        btnSol4 = (ImageButton) findViewById(R.id.btnSol4);
        btnSol5 = (ImageButton) findViewById(R.id.btnSol5);
        btnSol6 = (ImageButton) findViewById(R.id.btnSol6);
        btnSol7 = (ImageButton) findViewById(R.id.btnSol7);
        btnSol8 = (ImageButton) findViewById(R.id.btnSol8);
        btnSol9 = (ImageButton) findViewById(R.id.btnSol9);
        btnSol10 = (ImageButton) findViewById(R.id.btnSol10);
        btnSol11 = (ImageButton) findViewById(R.id.btnSol11);
        btnSol12 = (ImageButton) findViewById(R.id.btnSol12);
        btnSol13 = (ImageButton) findViewById(R.id.btnSol13);


        if (playerId == R.id.linearLayoutUst) {
            btnUst1.setOnTouchListener(new TouchListener());
            btnUst2.setOnTouchListener(new TouchListener());
            btnUst3.setOnTouchListener(new TouchListener());
            btnUst4.setOnTouchListener(new TouchListener());
            btnUst5.setOnTouchListener(new TouchListener());
            btnUst6.setOnTouchListener(new TouchListener());
            btnUst7.setOnTouchListener(new TouchListener());
            btnUst8.setOnTouchListener(new TouchListener());
            btnUst9.setOnTouchListener(new TouchListener());
            btnUst10.setOnTouchListener(new TouchListener());
            btnUst11.setOnTouchListener(new TouchListener());
            btnUst12.setOnTouchListener(new TouchListener());
            btnUst13.setOnTouchListener(new TouchListener());
            btnUst14.setOnTouchListener(new TouchListener());
            btnUst15.setOnTouchListener(new TouchListener());
            btnUst16.setOnTouchListener(new TouchListener());
            btnUst17.setOnTouchListener(new TouchListener());
            btnUst18.setOnTouchListener(new TouchListener());
            btnUst19.setOnTouchListener(new TouchListener());
            btnUst20.setOnTouchListener(new TouchListener());

            //TODO Animasyon için eklendi
            btnAlt1.setOnTouchListener(new TouchListener());
            btnAlt2.setOnTouchListener(new TouchListener());
            btnAlt3.setOnTouchListener(new TouchListener());
            btnAlt4.setOnTouchListener(new TouchListener());
            btnAlt5.setOnTouchListener(new TouchListener());
            btnAlt6.setOnTouchListener(new TouchListener());
            btnAlt7.setOnTouchListener(new TouchListener());
            btnAlt8.setOnTouchListener(new TouchListener());
            btnAlt9.setOnTouchListener(new TouchListener());
            btnAlt10.setOnTouchListener(new TouchListener());
            btnAlt11.setOnTouchListener(new TouchListener());
            btnAlt12.setOnTouchListener(new TouchListener());
            btnAlt13.setOnTouchListener(new TouchListener());
            btnAlt14.setOnTouchListener(new TouchListener());
            btnAlt15.setOnTouchListener(new TouchListener());
            btnAlt16.setOnTouchListener(new TouchListener());
            btnAlt17.setOnTouchListener(new TouchListener());
            btnAlt18.setOnTouchListener(new TouchListener());
            btnAlt19.setOnTouchListener(new TouchListener());
            btnAlt20.setOnTouchListener(new TouchListener());


        }
        if (playerId == R.id.linearLayoutSag) {
            btnSag1.setOnTouchListener(new TouchListener());
            btnSag2.setOnTouchListener(new TouchListener());
            btnSag3.setOnTouchListener(new TouchListener());
            btnSag4.setOnTouchListener(new TouchListener());
            btnSag5.setOnTouchListener(new TouchListener());
            btnSag6.setOnTouchListener(new TouchListener());
            btnSag7.setOnTouchListener(new TouchListener());
            btnSag8.setOnTouchListener(new TouchListener());
            btnSag9.setOnTouchListener(new TouchListener());
            btnSag10.setOnTouchListener(new TouchListener());
            btnSag11.setOnTouchListener(new TouchListener());
            btnSag12.setOnTouchListener(new TouchListener());
            btnSag13.setOnTouchListener(new TouchListener());
        }
        if (playerId == R.id.linearLayoutSol) {
            btnSol1.setOnTouchListener(new TouchListener());
            btnSol2.setOnTouchListener(new TouchListener());
            btnSol3.setOnTouchListener(new TouchListener());
            btnSol4.setOnTouchListener(new TouchListener());
            btnSol5.setOnTouchListener(new TouchListener());
            btnSol6.setOnTouchListener(new TouchListener());
            btnSol7.setOnTouchListener(new TouchListener());
            btnSol8.setOnTouchListener(new TouchListener());
            btnSol9.setOnTouchListener(new TouchListener());
            btnSol10.setOnTouchListener(new TouchListener());
            btnSol11.setOnTouchListener(new TouchListener());
            btnSol12.setOnTouchListener(new TouchListener());
            btnSol13.setOnTouchListener(new TouchListener());
        }
        if (playerId == R.id.linearLayoutAlt) {
            btnAlt1.setOnTouchListener(new TouchListener());
            btnAlt2.setOnTouchListener(new TouchListener());
            btnAlt3.setOnTouchListener(new TouchListener());
            btnAlt4.setOnTouchListener(new TouchListener());
            btnAlt5.setOnTouchListener(new TouchListener());
            btnAlt6.setOnTouchListener(new TouchListener());
            btnAlt7.setOnTouchListener(new TouchListener());
            btnAlt8.setOnTouchListener(new TouchListener());
            btnAlt9.setOnTouchListener(new TouchListener());
            btnAlt10.setOnTouchListener(new TouchListener());
            btnAlt11.setOnTouchListener(new TouchListener());
            btnAlt12.setOnTouchListener(new TouchListener());
            btnAlt13.setOnTouchListener(new TouchListener());
            btnAlt14.setOnTouchListener(new TouchListener());
            btnAlt15.setOnTouchListener(new TouchListener());
            btnAlt16.setOnTouchListener(new TouchListener());
            btnAlt17.setOnTouchListener(new TouchListener());
            btnAlt18.setOnTouchListener(new TouchListener());
            btnAlt19.setOnTouchListener(new TouchListener());
            btnAlt20.setOnTouchListener(new TouchListener());
        }

        if (oyuncuSayisi == 2) {
            linearLayoutUst.setVisibility(View.VISIBLE);
            linearLayoutAlt.setVisibility(View.VISIBLE);
            linearLayoutSag.setVisibility(View.INVISIBLE);
            linearLayoutSol.setVisibility(View.INVISIBLE);
        } else if (oyuncuSayisi == 3) {
            linearLayoutUst.setVisibility(View.VISIBLE);
            linearLayoutAlt.setVisibility(View.VISIBLE);
            linearLayoutSag.setVisibility(View.VISIBLE);
            linearLayoutSol.setVisibility(View.INVISIBLE);
        } else {
            linearLayoutUst.setVisibility(View.VISIBLE);
            linearLayoutAlt.setVisibility(View.VISIBLE);
            linearLayoutSag.setVisibility(View.VISIBLE);
            linearLayoutSol.setVisibility(View.VISIBLE);
        }
    }

    private void CleanTable() {
        findViewById(R.id.layoutCevap).findViewById(R.id.img1).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img2).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img3).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img4).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img5).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img6).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img7).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img8).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img9).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img10).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img11).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img12).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img13).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img14).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img15).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img16).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img17).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img18).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img19).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img20).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img21).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img22).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img23).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img24).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img25).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img26).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img27).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img28).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img29).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img30).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img31).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img32).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img33).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img34).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img35).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img36).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img37).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img38).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img39).setBackground(null);
        findViewById(R.id.layoutCevap).findViewById(R.id.img40).setBackground(null);


    }

    private void Events() {
        Button animasyon = (Button) findViewById(R.id.animasyon);
        animasyon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (kullaniciyaDagitilanKardlar.size() < 40) {
                    //TODO Diger kullanıcının attıgı kard firebase den gelecek
                    playerCardIdNewCardMove = btnUst1.getId();
                    findViewById(playerCardIdNewCardMove).setVisibility(View.INVISIBLE);

                    ImageView tempAnimation = null;
                    //TODO Diger kullanıcının id ve kard numarası firebase den gelecek
                    if (playerIdNewCardMove == R.id.linearLayoutUst)
                        tempAnimation = btnUstAnimasyon;
                    else if (playerIdNewCardMove == R.id.linearLayoutAlt)
                        tempAnimation = btnAltAnimasyon;
                    else if (playerIdNewCardMove == R.id.linearLayoutSag)
                        tempAnimation = btnSagAnimasyon;
                    else if (playerIdNewCardMove == R.id.linearLayoutSol)
                        tempAnimation = btnSolAnimasyon;



                    PuanVer(playerIdNewCardMove);
                    MasayaKardGonder(playerIdNewCardMove, findViewById(playerCardIdNewCardMove), playerCardNumberNewCardMove);
                    tempAnimation.setImageDrawable(getApplicationContext().getResources().getDrawable(R.drawable.kapat0001));
                    tempAnimation.setVisibility(View.VISIBLE);
                    AnimasyonaMasayaKardGonder(playerIdNewCardMove, tempAnimation.getId());

                } else {
                    Toast.makeText(getApplicationContext(), "Kard Bitti", Toast.LENGTH_SHORT).show();

                }


            }
        });


        Button kartCek = (Button) findViewById(R.id.kardCek);
        kartCek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (kullaniciyaDagitilanKardlar.size() < 40) {

                    KartCekildi(playerId, randomSayiGetir(1, AppConstants.cardCountOnTable));
                } else {
                    Toast.makeText(getApplicationContext(), "Kard Bitti", Toast.LENGTH_SHORT).show();

                }


            }
        });

    }
    ///////////////////****************************************************//////////////////////////////////


    ///////////////////******************Animasyon İşlemleri***************//////////////////////////////////
    private void AnimasyonaMasayaKardGonder(int playerIdAtilan, int kardId) {

        btnAnimasyon = (ImageView) findViewById(kardId);
        btnAnimasyon.setAlpha(1.0f);
        MasaninOrtaNoktasiniGetir(kardId);

        if (playerIdAtilan == R.id.linearLayoutUst)
            animationMasayaKardGonder = new TranslateAnimation(0, 0, 0, masaY);
        if (playerIdAtilan == R.id.linearLayoutAlt)
            animationMasayaKardGonder = new TranslateAnimation(0, 0, 0, -masaY);
        if (playerIdAtilan == R.id.linearLayoutSag)
            animationMasayaKardGonder = new TranslateAnimation(0, masaX, 0, 0);
        if (playerIdAtilan == R.id.linearLayoutSol)
            animationMasayaKardGonder = new TranslateAnimation(0, -masaX, 0, 0);
        animationMasayaKardGonder.setDuration(2500);
        btnAnimasyon.startAnimation(animationMasayaKardGonder);
        animationMasayaKardGonder.setFillAfter(true);
        animationMasayaKardGonder.setAnimationListener(mAnimationListener);
    }

    private void MasaninOrtaNoktasiniGetir(int kardId) {
        View view = findViewById(R.id.layoutCevap);
        int[] location = new int[2];
        view.getLocationInWindow(location);
        masaX = location[0] + view.getWidth() / 2;
        masaY = location[1] + view.getHeight() / 2;
    }

    private Animation.AnimationListener mAnimationListener = new Animation.AnimationListener() {

        @Override
        public void onAnimationStart(Animation animation) {
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {


            new android.os.Handler().postDelayed
                    (
                            new Runnable() {
                                public void run() {


                                    btnAnimasyon.setVisibility(View.INVISIBLE);
                                    animationMasayaKardGonder.cancel();
                                    animationMasayaKardGonder.setAnimationListener(null);

                                    // animation2 = new TranslateAnimation(cevapX, location[0],cevapY,
                                    //       location[1]);
                                    //animation2.setDuration(2000);
                                    //hand.startAnimation(animation2);
                                    //animation2.setFillAfter(true);
                                    //animation2.setAnimationListener(mAnimationListener2);
                                }
                            },
                            3000);

        }
    };

    ///////////////////****************************************************//////////////////////////////////


    ///Kullanıcıların sayısına göre kullanıclara kar dagıtılır.Main kullanıcı ilkkez set eder(ustDizi,altDizi,sagDizi,solDizi)
    private void FirstSetPlayersCardCount() {
        kullanilanKardlar.clear();
        kullanilanEnsonKard = 0;

        kullaniciyaDagitilanKardlar.clear();

        if (playerCount == 2) {
                /*Üst Taraftaki kullanıcıya random kard(yeni kard) getirir.*/
            ustDizi = new int[]{randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty};

            //Alt Taraftaki kullanıcıya random kard(yeni kard) getirir.
            altDizi = new int[]{randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty};

        }
        if (playerCount == 3) {
                 /*Üst Taraftaki kullanıcıya random kard(yeni kard) getirir.*/
            ustDizi = new int[]{randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty};

            //Alt Taraftaki kullanıcıya random kard(yeni kard) getirir.
            altDizi = new int[]{randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty};

            //Sag Taraftaki kullanıcıya random kard(yeni kard) getirir.
            sagDizi = new int[]{randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty};


        }
        if (playerCount == 4) {
               /*Üst Taraftaki kullanıcıya random kard(yeni kard) getirir.*/
            ustDizi = new int[]{randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty};

            //Alt Taraftaki kullanıcıya random kard(yeni kard) getirir.
            altDizi = new int[]{randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty};

            //Sag Taraftaki kullanıcıya random kard(yeni kard) getirir.
            sagDizi = new int[]{randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty};


            //Sol Taraftaki kullanıcıya random kard(yeni kard) getirir.
            solDizi = new int[]{randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), randomSayiGetir(1, AppConstants.cardCountOnTable), AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty, AppConstants.playerCartEmty};

        }


        SetPlayersCards();


    }

    private void SetPlayersCards() {


        selectionSort(ustDizi, playerGameCartCount);
        kardGetir(btnUst1, ustDizi[0]);
        kardGetir(btnUst2, ustDizi[1]);
        kardGetir(btnUst3, ustDizi[2]);
        kardGetir(btnUst4, ustDizi[3]);
        kardGetir(btnUst5, ustDizi[4]);
        kardGetir(btnUst6, ustDizi[5]);
        kardGetir(btnUst7, ustDizi[6]);
        kardGetir(btnUst8, ustDizi[7]);
        kardGetir(btnUst9, ustDizi[8]);
        kardGetir(btnUst10, ustDizi[9]);
        kardGetir(btnUst11, ustDizi[10]);
        kardGetir(btnUst12, ustDizi[11]);
        kardGetir(btnUst13, ustDizi[12]);
        kardGetir(btnUst14, ustDizi[13]);
        //TODO Oyuncu sayısı 2 den fazla oldugunda sadece bu oyuncu 14 kard alabiliyor digerleri 13 kard alır
        if (playerGameCartCount == 20) {
            kardGetir(btnUst15, ustDizi[14]);
            kardGetir(btnUst16, ustDizi[15]);
            kardGetir(btnUst17, ustDizi[16]);
            kardGetir(btnUst18, ustDizi[17]);
            kardGetir(btnUst19, ustDizi[18]);
            kardGetir(btnUst20, ustDizi[19]);
        }


        selectionSort(altDizi, playerGameCartCount);
        kardGetir(btnAlt1, altDizi[0]);
        kardGetir(btnAlt2, altDizi[1]);
        kardGetir(btnAlt3, altDizi[2]);
        kardGetir(btnAlt4, altDizi[3]);
        kardGetir(btnAlt5, altDizi[4]);
        kardGetir(btnAlt6, altDizi[5]);
        kardGetir(btnAlt7, altDizi[6]);
        kardGetir(btnAlt8, altDizi[7]);
        kardGetir(btnAlt9, altDizi[8]);
        kardGetir(btnAlt10, altDizi[9]);
        kardGetir(btnAlt11, altDizi[10]);
        kardGetir(btnAlt12, altDizi[11]);
        kardGetir(btnAlt13, altDizi[12]);
        if (playerGameCartCount == 20) {
            kardGetir(btnAlt14, altDizi[13]);
            kardGetir(btnAlt15, altDizi[14]);
            kardGetir(btnAlt16, altDizi[15]);
            kardGetir(btnAlt17, altDizi[16]);
            kardGetir(btnAlt18, altDizi[17]);
            kardGetir(btnAlt19, altDizi[18]);
            kardGetir(btnAlt20, altDizi[19]);
        }


        if (playerCount > 2) {
            selectionSort(sagDizi, playerGameCartCount);
            kardGetir(btnSag1, sagDizi[0]);
            kardGetir(btnSag2, sagDizi[1]);
            kardGetir(btnSag3, sagDizi[2]);
            kardGetir(btnSag4, sagDizi[3]);
            kardGetir(btnSag5, sagDizi[4]);
            kardGetir(btnSag6, sagDizi[5]);
            kardGetir(btnSag7, sagDizi[6]);
            kardGetir(btnSag8, sagDizi[7]);
            kardGetir(btnSag9, sagDizi[8]);
            kardGetir(btnSag10, sagDizi[9]);
            kardGetir(btnSag11, sagDizi[10]);
            kardGetir(btnSag12, sagDizi[11]);
            kardGetir(btnSag13, sagDizi[12]);

        }
        if (playerCount == 4) {
            selectionSort(solDizi, playerGameCartCount);
            kardGetir(btnSol1, solDizi[0]);
            kardGetir(btnSol2, solDizi[1]);
            kardGetir(btnSol3, solDizi[2]);
            kardGetir(btnSol4, solDizi[3]);
            kardGetir(btnSol5, solDizi[4]);
            kardGetir(btnSol6, solDizi[5]);
            kardGetir(btnSol7, solDizi[6]);
            kardGetir(btnSol8, solDizi[7]);
            kardGetir(btnSol9, solDizi[8]);
            kardGetir(btnSol10, solDizi[9]);
            kardGetir(btnSol11, solDizi[10]);
            kardGetir(btnSol12, solDizi[11]);
            kardGetir(btnSol13, solDizi[12]);
            kardGetir(btnSol13, solDizi[13]);
        }


    }


    private int randomSayiGetir(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;

        for (Integer deger : kullaniciyaDagitilanKardlar) {
            if (randomNum == deger) return randomSayiGetir(1, AppConstants.cardCountOnTable);

        }
        kullaniciyaDagitilanKardlar.add(randomNum);
        return randomNum;
    }


    private void selectionSort(int[] dizi, int n) {
        int temp;
        int enKucuk;
        for (int i = 0; i < n - 1; i++) {
            enKucuk = i;

            for (int j = i; j < n; j++) {
                if (dizi[j] < dizi[enKucuk]) enKucuk = j;
            }
            temp = dizi[i];
            dizi[i] = dizi[enKucuk];
            dizi[enKucuk] = temp;


        }
    }

    private void kardGetir(View view, int name) {
        if (name == 1)
            view.setBackground(this.getResources().getDrawable(R.drawable.a1));
        if (name == 2)
            view.setBackground(this.getResources().getDrawable(R.drawable.a2));
        if (name == 3)
            view.setBackground(this.getResources().getDrawable(R.drawable.a3));
        if (name == 4)
            view.setBackground(this.getResources().getDrawable(R.drawable.a4));
        if (name == 5)
            view.setBackground(this.getResources().getDrawable(R.drawable.a5));
        if (name == 6)
            view.setBackground(this.getResources().getDrawable(R.drawable.a6));
        if (name == 7)
            view.setBackground(this.getResources().getDrawable(R.drawable.a7));
        if (name == 8)
            view.setBackground(this.getResources().getDrawable(R.drawable.a8));
        if (name == 9)
            view.setBackground(this.getResources().getDrawable(R.drawable.a9));
        if (name == 10)
            view.setBackground(this.getResources().getDrawable(R.drawable.a10));
        if (name == 11)
            view.setBackground(this.getResources().getDrawable(R.drawable.a11));
        if (name == 12)
            view.setBackground(this.getResources().getDrawable(R.drawable.a12));
        if (name == 13)
            view.setBackground(this.getResources().getDrawable(R.drawable.a13));
        if (name == 14)
            view.setBackground(this.getResources().getDrawable(R.drawable.a14));
        if (name == 15)
            view.setBackground(this.getResources().getDrawable(R.drawable.a15));
        if (name == 16)
            view.setBackground(this.getResources().getDrawable(R.drawable.a16));
        if (name == 17)
            view.setBackground(this.getResources().getDrawable(R.drawable.a17));
        if (name == 18)
            view.setBackground(this.getResources().getDrawable(R.drawable.a18));
        if (name == 19)
            view.setBackground(this.getResources().getDrawable(R.drawable.a19));
        if (name == 20)
            view.setBackground(this.getResources().getDrawable(R.drawable.a20));
        if (name == 21)
            view.setBackground(this.getResources().getDrawable(R.drawable.a21));
        if (name == 22)
            view.setBackground(this.getResources().getDrawable(R.drawable.a22));
        if (name == 23)
            view.setBackground(this.getResources().getDrawable(R.drawable.a23));
        if (name == 24)
            view.setBackground(this.getResources().getDrawable(R.drawable.a24));
        if (name == 25)
            view.setBackground(this.getResources().getDrawable(R.drawable.a25));
        if (name == 26)
            view.setBackground(this.getResources().getDrawable(R.drawable.a26));
        if (name == 27)
            view.setBackground(this.getResources().getDrawable(R.drawable.a27));
        if (name == 28)
            view.setBackground(this.getResources().getDrawable(R.drawable.a28));
        if (name == 29)
            view.setBackground(this.getResources().getDrawable(R.drawable.a29));
        if (name == 30)
            view.setBackground(this.getResources().getDrawable(R.drawable.a30));
        if (name == 31)
            view.setBackground(this.getResources().getDrawable(R.drawable.a31));
        if (name == 32)
            view.setBackground(this.getResources().getDrawable(R.drawable.a32));
        if (name == 33)
            view.setBackground(this.getResources().getDrawable(R.drawable.a33));
        if (name == 34)
            view.setBackground(this.getResources().getDrawable(R.drawable.a34));
        if (name == 35)
            view.setBackground(this.getResources().getDrawable(R.drawable.a35));
        if (name == 36)
            view.setBackground(this.getResources().getDrawable(R.drawable.a36));
        if (name == 37)
            view.setBackground(this.getResources().getDrawable(R.drawable.a37));
        if (name == 38)
            view.setBackground(this.getResources().getDrawable(R.drawable.a38));
        if (name == 39)
            view.setBackground(this.getResources().getDrawable(R.drawable.a39));
        if (name == 40)
            view.setBackground(this.getResources().getDrawable(R.drawable.a40));


        if (name == 100)
            view.setBackground(null);
        view.setTag(Integer.toString(name));
        view.setVisibility(View.VISIBLE);


    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ///////////////////******************Masaya Kard gönderme işlemleri***************//////////////////////////////////////////////
    private void MasayaKardGonder(int playerIdAtilan, View view, int name) {
        //Label olayı yemedi şimdilik Toast yaptım :)
        //Toast.makeText(this, "Card Index: " + Integer.toString(name), Toast.LENGTH_SHORT).show();

        //Mücahit
        Card kart = new Card(name);
        //GameManager.ins.myNewCardMovement(kart,GameManager.ins.getActiveGame().getMovements().size());

        boolean kontrol = true;


        if (kullanilanEnsonKard != 0) {
            if ((name + 1 == kullanilanEnsonKard) || (name - 1 == kullanilanEnsonKard))
                kontrol = true;
            else {
                kontrol = false;
                for (Integer deger : kullanilanKardlar) {
                    if ((deger + 1 == name) || (deger - 1 == name)) kontrol = true;

                }
            }
        }
        if (kontrol == true) {
            kullanilanEnsonKard = name;
            kullanilanKardlar.add(name);

            LinearLayout owner = (LinearLayout) view.getParent();

            view.setVisibility(View.INVISIBLE);
            //view.setOnTouchListener(null);
            PuanVer(owner.getId());
            KartAtildi(playerIdAtilan, kullanilanEnsonKard);
            if (name == 1) {
                layoutCevap.findViewById(R.id.img1).setBackground(this.getResources().getDrawable(R.drawable.b1));
                layoutCevap.findViewById(R.id.img1).setTag(Integer.toString(name));
            }
            if (name == 2) {
                layoutCevap.findViewById(R.id.img2).setBackground(this.getResources().getDrawable(R.drawable.b2));
                layoutCevap.findViewById(R.id.img2).setTag(Integer.toString(name));
            }
            if (name == 3) {
                layoutCevap.findViewById(R.id.img3).setBackground(this.getResources().getDrawable(R.drawable.b3));
                layoutCevap.findViewById(R.id.img3).setTag(Integer.toString(name));
            }
            if (name == 4) {
                layoutCevap.findViewById(R.id.img4).setBackground(this.getResources().getDrawable(R.drawable.b4));
                layoutCevap.findViewById(R.id.img4).setTag(Integer.toString(name));
            }
            if (name == 5) {
                layoutCevap.findViewById(R.id.img5).setBackground(this.getResources().getDrawable(R.drawable.b5));
                layoutCevap.findViewById(R.id.img5).setTag(Integer.toString(name));
            }

            if (name == 6) {
                layoutCevap.findViewById(R.id.img6).setBackground(this.getResources().getDrawable(R.drawable.b6));
                layoutCevap.findViewById(R.id.img6).setTag(Integer.toString(name));
            }
            if (name == 7) {
                layoutCevap.findViewById(R.id.img7).setBackground(this.getResources().getDrawable(R.drawable.b7));
                layoutCevap.findViewById(R.id.img7).setTag(Integer.toString(name));
            }
            if (name == 8) {
                layoutCevap.findViewById(R.id.img8).setBackground(this.getResources().getDrawable(R.drawable.b8));
                layoutCevap.findViewById(R.id.img8).setTag(Integer.toString(name));
            }
            if (name == 9) {
                layoutCevap.findViewById(R.id.img9).setBackground(this.getResources().getDrawable(R.drawable.b9));
                layoutCevap.findViewById(R.id.img9).setTag(Integer.toString(name));
            }
            if (name == 10) {
                layoutCevap.findViewById(R.id.img10).setBackground(this.getResources().getDrawable(R.drawable.b10));
                layoutCevap.findViewById(R.id.img10).setTag(Integer.toString(name));
            }
            if (name == 11) {
                layoutCevap.findViewById(R.id.img11).setBackground(this.getResources().getDrawable(R.drawable.b11));
                layoutCevap.findViewById(R.id.img11).setTag(Integer.toString(name));
            }
            if (name == 12) {
                layoutCevap.findViewById(R.id.img12).setBackground(this.getResources().getDrawable(R.drawable.b12));
                layoutCevap.findViewById(R.id.img12).setTag(Integer.toString(name));
            }
            if (name == 13) {
                layoutCevap.findViewById(R.id.img13).setBackground(this.getResources().getDrawable(R.drawable.b13));
                layoutCevap.findViewById(R.id.img13).setTag(Integer.toString(name));

            }
            if (name == 14) {
                layoutCevap.findViewById(R.id.img14).setBackground(this.getResources().getDrawable(R.drawable.b14));
                layoutCevap.findViewById(R.id.img14).setTag(Integer.toString(name));

            }
            if (name == 15) {
                layoutCevap.findViewById(R.id.img15).setBackground(this.getResources().getDrawable(R.drawable.b15));
                layoutCevap.findViewById(R.id.img15).setTag(Integer.toString(name));
            }
            if (name == 16) {
                layoutCevap.findViewById(R.id.img16).setBackground(this.getResources().getDrawable(R.drawable.b16));
                layoutCevap.findViewById(R.id.img16).setTag(Integer.toString(name));
            }
            if (name == 17) {
                layoutCevap.findViewById(R.id.img17).setBackground(this.getResources().getDrawable(R.drawable.b17));
                layoutCevap.findViewById(R.id.img17).setTag(Integer.toString(name));
            }
            if (name == 18) {
                layoutCevap.findViewById(R.id.img18).setBackground(this.getResources().getDrawable(R.drawable.b18));
                layoutCevap.findViewById(R.id.img18).setTag(Integer.toString(name));
            }
            if (name == 19) {
                layoutCevap.findViewById(R.id.img19).setBackground(this.getResources().getDrawable(R.drawable.b19));
                layoutCevap.findViewById(R.id.img19).setTag(Integer.toString(name));
            }
            if (name == 20) {
                layoutCevap.findViewById(R.id.img20).setBackground(this.getResources().getDrawable(R.drawable.b20));
                layoutCevap.findViewById(R.id.img20).setTag(Integer.toString(name));
            }
            if (name == 21) {
                layoutCevap.findViewById(R.id.img21).setBackground(this.getResources().getDrawable(R.drawable.b21));
                layoutCevap.findViewById(R.id.img21).setTag(Integer.toString(name));
            }
            if (name == 22) {
                layoutCevap.findViewById(R.id.img22).setBackground(this.getResources().getDrawable(R.drawable.b22));
                layoutCevap.findViewById(R.id.img22).setTag(Integer.toString(name));
            }
            if (name == 23) {
                layoutCevap.findViewById(R.id.img23).setBackground(this.getResources().getDrawable(R.drawable.b23));
                layoutCevap.findViewById(R.id.img23).setTag(Integer.toString(name));
            }
            if (name == 24) {
                layoutCevap.findViewById(R.id.img24).setBackground(this.getResources().getDrawable(R.drawable.b24));
                layoutCevap.findViewById(R.id.img24).setTag(Integer.toString(name));
            }
            if (name == 25) {
                layoutCevap.findViewById(R.id.img25).setBackground(this.getResources().getDrawable(R.drawable.b25));
                layoutCevap.findViewById(R.id.img25).setTag(Integer.toString(name));
            }
            if (name == 26) {
                layoutCevap.findViewById(R.id.img26).setBackground(this.getResources().getDrawable(R.drawable.b26));
                layoutCevap.findViewById(R.id.img26).setTag(Integer.toString(name));
            }
            if (name == 27) {
                layoutCevap.findViewById(R.id.img27).setBackground(this.getResources().getDrawable(R.drawable.b27));
                layoutCevap.findViewById(R.id.img27).setTag(Integer.toString(name));
            }
            if (name == 28) {
                layoutCevap.findViewById(R.id.img28).setBackground(this.getResources().getDrawable(R.drawable.b28));
                layoutCevap.findViewById(R.id.img28).setTag(Integer.toString(name));
            }
            if (name == 29) {
                layoutCevap.findViewById(R.id.img29).setBackground(this.getResources().getDrawable(R.drawable.b29));
                layoutCevap.findViewById(R.id.img29).setTag(Integer.toString(name));
            }
            if (name == 30) {
                layoutCevap.findViewById(R.id.img30).setBackground(this.getResources().getDrawable(R.drawable.b30));
                layoutCevap.findViewById(R.id.img30).setTag(Integer.toString(name));
            }
            if (name == 31) {
                layoutCevap.findViewById(R.id.img31).setBackground(this.getResources().getDrawable(R.drawable.b31));
                layoutCevap.findViewById(R.id.img31).setTag(Integer.toString(name));
            }
            if (name == 32) {
                layoutCevap.findViewById(R.id.img32).setBackground(this.getResources().getDrawable(R.drawable.b32));
                layoutCevap.findViewById(R.id.img32).setTag(Integer.toString(name));
            }
            if (name == 33) {
                layoutCevap.findViewById(R.id.img33).setBackground(this.getResources().getDrawable(R.drawable.b33));
                layoutCevap.findViewById(R.id.img33).setTag(Integer.toString(name));
            }
            if (name == 34) {
                layoutCevap.findViewById(R.id.img34).setBackground(this.getResources().getDrawable(R.drawable.b34));
                layoutCevap.findViewById(R.id.img34).setTag(Integer.toString(name));
            }
            if (name == 35) {
                layoutCevap.findViewById(R.id.img35).setBackground(this.getResources().getDrawable(R.drawable.b35));
                layoutCevap.findViewById(R.id.img35).setTag(Integer.toString(name));
            }
            if (name == 36) {
                layoutCevap.findViewById(R.id.img36).setBackground(this.getResources().getDrawable(R.drawable.b36));
                layoutCevap.findViewById(R.id.img36).setTag(Integer.toString(name));
            }
            if (name == 37) {
                layoutCevap.findViewById(R.id.img37).setBackground(this.getResources().getDrawable(R.drawable.b37));
                layoutCevap.findViewById(R.id.img37).setTag(Integer.toString(name));
            }
            if (name == 38) {
                layoutCevap.findViewById(R.id.img38).setBackground(this.getResources().getDrawable(R.drawable.b38));
                layoutCevap.findViewById(R.id.img38).setTag(Integer.toString(name));
            }
            if (name == 39) {
                layoutCevap.findViewById(R.id.img39).setBackground(this.getResources().getDrawable(R.drawable.b39));
                layoutCevap.findViewById(R.id.img39).setTag(Integer.toString(name));
            }
            if (name == 40) {
                layoutCevap.findViewById(R.id.img40).setBackground(this.getResources().getDrawable(R.drawable.b40));
                layoutCevap.findViewById(R.id.img40).setTag(Integer.toString(name));
            }


            //TODO Kullanıcının attıgı kard burdan diger kullanicalara gönderilir
            if (playerIdAtilan == playerId) {
                int fireBaseGidecekKardNumber = name;
                int fireBaseGidecekKardId = view.getId();
                int fireBaseGidecekPlayerId = playerId;

            }
        }


    }

    private void KartAtildi(int playerIdAtilan, int atilanKard) {
        if (playerIdAtilan == R.id.linearLayoutUst) {
            for (int i = 0; i < ustDizi.length; i++) {
                if (ustDizi[i] == atilanKard) {
                    ustDizi[i] = 100;
                    break;
                }

            }
            //TODO Animasyon için eklendi
            for (int i = 0; i < altDizi.length; i++) {
                if (altDizi[i] == atilanKard) {
                    altDizi[i] = 100;
                    break;
                }

            }

        }
        if (playerIdAtilan == R.id.linearLayoutAlt) {
            for (int i = 0; i < altDizi.length; i++) {
                if (altDizi[i] == atilanKard) {
                    altDizi[i] = 100;
                    break;
                }

            }
        }
        if (playerIdAtilan == R.id.linearLayoutSag) {
            for (int i = 0; i < sagDizi.length; i++) {
                if (sagDizi[i] == atilanKard) {
                    sagDizi[i] = 100;
                    break;
                }

            }
        }
        if (playerIdAtilan == R.id.linearLayoutSol) {
            for (int i = 0; i < solDizi.length; i++) {
                if (solDizi[i] == atilanKard) {
                    solDizi[i] = 100;
                    break;
                }

            }
        }
        SetPlayersCards();
    }

    int kontrolKartCekildi=0;
    private void KartCekildi(int playerIdCekilen, int cekilenKard) {


        if (playerIdCekilen == R.id.linearLayoutUst)
        {
            //TODO Animasyon için eklendi
            if (kontrolKartCekildi==0)
            {
                for (int i = 0; i < ustDizi.length; i++) {
                    if (ustDizi[i] == 100) {
                        ustDizi[i] = cekilenKard;
                        break;
                    }

                }
                kontrolKartCekildi=1;
            }
            else
            {
                for (int i = 0; i < altDizi.length; i++) {
                    if (altDizi[i] == 100) {
                        altDizi[i] = cekilenKard;
                        break;
                    }
                    kontrolKartCekildi=0;
            }



            }


        } else if (playerIdCekilen == R.id.linearLayoutAlt) {
            for (int i = 0; i < altDizi.length; i++) {
                if (altDizi[i] == 100) {
                    altDizi[i] = cekilenKard;
                    break;
                }

            }
        } else if (playerIdCekilen == R.id.linearLayoutSag) {
            for (int i = 0; i < sagDizi.length; i++) {
                if (sagDizi[i] == 100) {
                    sagDizi[i] = cekilenKard;
                    break;
                }

            }
        } else if (playerIdCekilen == R.id.linearLayoutSol) {
            for (int i = 0; i < solDizi.length; i++) {
                if (solDizi[i] == 100) {
                    solDizi[i] = cekilenKard;
                    break;
                }

            }
        }
        SetPlayersCards();
    }

    private void PuanVer(int playerIdAtilan) {
        Integer puan = 0;
        if (playerIdAtilan == R.id.linearLayoutUst) {

            puan = Integer.parseInt(ustPuan.getText().toString());
            puan = puan + 10;
            ustPuan.setText(puan.toString());
        } else if (playerIdAtilan == R.id.linearLayoutSag) {
            puan = Integer.parseInt(sagPuan.getText().toString());
            puan = puan + 10;
            sagPuan.setText(puan.toString());
        } else if (playerIdAtilan == R.id.linearLayoutSol) {
            puan = Integer.parseInt(solPuan.getText().toString());
            puan = puan + 10;
            solPuan.setText(puan.toString());
        } else if (playerIdAtilan == R.id.linearLayoutAlt) {
            puan = Integer.parseInt(altPuan.getText().toString());
            puan = puan + 10;
            altPuan.setText(puan.toString());
        }

    }
/*
    @Override
    public void newCardMove(Movement movement) {
        Log.d("Listener", "New Card " + movement);
        //Bi kullanıcı kart attığında buraya düşecek.
        //TODO: aktif oyuncu kimse attığı kartın animasyonu burda çalıştırılmalı

        if (movement.getMoveIndex() >= AppConstants.cardCountOnTable) {
            //Oyun bitti
            finish();
        } else {
            int playerIndex = (movement.getMoveIndex() + 1) % AppConstants.playerCount;
            if (playerIndex == GameManager.ins.me.getpIndex()) {
                Log.d("", "My Turn");
                GameManager.ins.myNewCardMovement(new Card(23),movement.getMoveIndex()+1);
            } else {
                Log.d("", "Other Player Turn: " + playerIndex);
            }
        }
    }
*/
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    private final class TouchListener implements OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {


                ClipData data = ClipData.newPlainText("", "");
                DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
                v.startDrag(data, shadowBuilder, v, 0);
                //v.setVisibility(View.INVISIBLE);
                //v.setOnTouchListener(null);
                return true;
            }
            return false;
        }
    }

    class DragListener implements OnDragListener {
        Drawable enterShape = getResources().getDrawable(R.drawable.masa);
        Drawable normalShape = getResources().getDrawable(R.drawable.masa);

        @Override
        public boolean onDrag(View v, DragEvent event) {
            int action = event.getAction();
            switch (action) {
                case DragEvent.ACTION_DRAG_STARTED:
                    // do nothing
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    v.setBackgroundDrawable(enterShape);
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    v.setBackgroundDrawable(normalShape);
                    break;
                case DragEvent.ACTION_DROP:
                    // Dropped, reassign View to ViewGroup
                    View view = (View) event.getLocalState();
                    ViewGroup owner = (ViewGroup) view.getParent();
                    String backgroundImageName = (String) view.getTag();
                    v.setOnTouchListener(null);
                    Integer kardAdi = Integer.parseInt(backgroundImageName);

                    MasayaKardGonder(playerId, view, kardAdi);

                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    v.setBackgroundDrawable(normalShape);
                    break;
                default:
                    break;
            }
            return true;
        }

    }


}
