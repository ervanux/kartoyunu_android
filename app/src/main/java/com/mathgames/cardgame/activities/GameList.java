package com.mathgames.cardgame.activities;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.mathgames.cardgame.R;
import com.mathgames.cardgame.interfaces.GameListListener;
import com.mathgames.cardgame.managers.GameManager;
import com.mathgames.cardgame.models.Game;
import com.mathgames.cardgame.models.Player;

import java.util.ArrayList;
import java.util.Map;

public class GameList extends ParentActivity implements GameListListener {
    CustomGridAdapter adapter;

    @Override
    public void gameListUpdated() {
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_list);

        final GridView gridview = (GridView) findViewById(R.id.gridview);
        adapter = new CustomGridAdapter(this, GameManager.ins.games);
        gridview.setAdapter(adapter);


        Button button = (Button) findViewById(R.id.selectGameButton);
        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                GameManager.ins.joinOneOrCreateNewGame();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        GameManager.ins.setGameListener(this);
    }

    public class CustomGridAdapter extends BaseAdapter {

        private Context context;
        private ArrayList<Game> games;
        LayoutInflater inflater;

        public CustomGridAdapter(Context context, ArrayList<Game> games) {
            this.context = context;
            this.games = games;
            inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.cell, null);
            }
            Game game = this.games.get(position);
            if (game.getStatus() == 2) {
                convertView.setBackgroundColor(Color.DKGRAY);
            } else {
                convertView.setBackgroundColor(Color.LTGRAY);
            }

            TextView[] textViews = new TextView[] {
                    (TextView) convertView.findViewById(R.id.text1),
                    (TextView) convertView.findViewById(R.id.text2),
                    (TextView) convertView.findViewById(R.id.text3),
                    (TextView) convertView.findViewById(R.id.text4)
            };

            for (TextView textView : textViews) {
                textView.setText("");
            }

            Map<String, Player> players = game.getPlayers();
            if (players != null) {
                int i = 0;
                for (Player player : players.values()) {
                    //FIX
                    if (i == 4) {
                        break;
                    }
                    textViews[i].setText(player.getName());
                    i++;
                    if (i > 3) {
                        Button button = (Button) convertView.findViewById(R.id.cellButton);
                        button.setText("İzle");
                    } else {
                        Button button = (Button) convertView.findViewById(R.id.cellButton);
                        button.setText("Katıl");
                    }
                }
            }

            return convertView;
        }

        @Override
        public int getCount() {
            return games.size();
        }

        @Override
        public Object getItem(int position) {
            return games.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }
}