package com.mathgames.cardgame.utils;

import android.content.Context;
import android.provider.Settings;

import java.util.Random;
import java.util.UUID;

/**
 * Created by eugurlu on 06/03/16.
 */
public class Utils {

    public static String randomString(int size){
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }


    public static String randomUUID(){
        return UUID.randomUUID().toString();
    }

    public static String getAndroidUUID(Context context){
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }
}
