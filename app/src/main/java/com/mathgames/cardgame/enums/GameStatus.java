package com.mathgames.cardgame.enums;

/**
 * Created by eugurlu on 06/03/16.
 */
public enum GameStatus {
    WAITING_FOR_START(1), WAITING_FOR_CARDS(2), STARTED(3);
    private final int value;

    private GameStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
