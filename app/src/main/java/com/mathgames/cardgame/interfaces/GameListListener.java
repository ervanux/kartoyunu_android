package com.mathgames.cardgame.interfaces;

/**
 * Created by eugurlu on 26/03/16.
 */
public interface GameListListener {
    void gameListUpdated();
}
