package com.mathgames.cardgame.interfaces;

import com.mathgames.cardgame.models.Movement;

/**
 * Created by eugurlu on 13/03/16.
 */
public interface CardMoveListener {
    void newCardMove(Movement movement);
}
